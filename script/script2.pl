#!/usr/bin/perl

# Deteccio de usuaris inactius

$numArgs = @ARGV;
$p = 0;
$usage = "Usage: Script2.pl -t time\n";

if ($numArgs == 2) {
	if ($ARGV[0] ne "-t") {
		print $usage;
		exit (1);
	}
} else {
	print $usage;
	exit (1);
}

$time = $ARGV[1];
if (length($time) < 2) {
	print $usage;
	exit (1);
}

$last_char = substr($time, -1);
$num = substr($time, 0, length($time) - 1);
$days = 0;

if ($last_char eq "d") {
	$days = $num * 1;
} elsif ($last_char eq "w") {
	$days = $num * 7;
} elsif ($last_char eq "m") {
	$days = $num * 30;
} elsif ($last_char eq "y") {
	$days = $num * 365;
} else {
	print $usage;
	exit (1);
}

$pass_db_file = "/etc/passwd";
open(FILE, $pass_db_file) or die "no es pot obrir el fitxer $pass_db_file: $!";
@password_db = <FILE>; # llegir tot el fitxer de password
close FILE;

foreach $user_line (@password_db) {
	chomp($user_line); # eliminar el salt de línia

	@fields = split(':', $user_line);

	$user_id = $fields[0];
	$user_home = $fields[5];

	# Si te algun procés, continuem
	my $has_process = 0;
	@process_list = `ps aux --no-headers`;
	foreach $process_list_line (@process_list) {
		chomp($process_list_line);
		@fields_proc = split(" +" , $process_list_line);
		$user_proc = $fields_proc[0];
		if ($user_id eq $user_proc) {
			$has_process = 1;
			last;
		}
	}

	if ($has_process == 1) {
		next;
	}

	# Check last login
	my $has_last_login = 0;
	foreach $log_line (`lastlog -b $days`) {
		chomp($log_line);
		@fields_log = split(" +" , $log_line);
		if ($has_last_login eq $user_proc) {
			$has_last_login = 1;
			last;
		}
	}

	if ($has_last_login == 1) {
		next;
	}

	# Check last time a file was modified (in /home/$user)
	my $mod = `find /home/"$user_id" -mtime -$days 2> /dev/null`;
	if ($mod) {
		next;
	}


	print "$user_id\n";

}
